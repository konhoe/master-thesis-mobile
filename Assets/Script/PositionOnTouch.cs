﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class PositionOnTouch : MonoBehaviour
{
    public bool isLocked;
    [SerializeField] private GameObject toPosition, lockedUi, unlockedUi;
    [SerializeField] private TextMeshProUGUI lockText;
    private ARRaycastManager raycastManager;
    private ARPlaneManager planeManager;
    private ARPointCloudManager cloudManager;
    private List<ARRaycastHit> hitResults = new List<ARRaycastHit>();
    private StepTutorial steps;
    private TouchMode mode;

    private float distance = 0;

    private void Start()
    {
        steps = FindObjectOfType<StepTutorial>();
        raycastManager = FindObjectOfType<ARRaycastManager>();
        planeManager = FindObjectOfType<ARPlaneManager>();
        cloudManager = FindObjectOfType<ARPointCloudManager>();
        planeManager.planesChanged += PlanesChanged;
    }

    private void PlanesChanged(ARPlanesChangedEventArgs planes)
    {
        if(planes.added.Count > 0)
        {
            steps.ShowStep(2);
            planeManager.planesChanged -= PlanesChanged;
            planeManager.enabled = false;
            cloudManager.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        mode = GetTouchMode();
        if(mode == TouchMode.Position)
        {
            distance = 0;
            var screenPosition = Input.GetTouch(0).position;

            raycastManager.Raycast(screenPosition, hitResults, TrackableType.Planes);
            if(hitResults.Count > 0)
            {
                if (!toPosition.activeInHierarchy) toPosition.SetActive(true);
                var positionResult = hitResults[0].pose.position;
                toPosition.transform.position = positionResult;
                steps.ShowStep(3);
            }
        }else if(mode == TouchMode.RotateZoom)
        {
            var f0 = Input.GetTouch(0);
            var f1 = Input.GetTouch(1);
            //Scale
            var newDistance = (f0.position - f1.position).magnitude;
            if (distance == 0)
            {
                distance = newDistance;
            }else
            {
                toPosition.transform.localScale *= newDistance / distance;
            }
            distance = newDistance;

            //rotate https://answers.unity.com/questions/823319/2-fingers-sprite-rotation.html
            var prevPos0 = f0.position - f0.deltaPosition;  // Generate previous frame's finger positions
            var prevPos1 = f1.position - f1.deltaPosition;

            var prevDir = prevPos1 - prevPos0;
            var currDir = f1.position - f0.position;
            var angle = Vector2.SignedAngle(currDir, prevDir);
            toPosition.transform.Rotate(0, angle, 0);  // Rotate by the deltaAngle between the two vectors
        }
        else
        {
            distance = 0;
        }
    }

    private TouchMode GetTouchMode()
    {
        //Prevent jumping to last touching finger after Rotating/Scaling the object
        if(mode == TouchMode.RotateZoom && Input.touchCount == 1 && Input.GetTouch(0).phase != TouchPhase.Began)
        {
            return TouchMode.None;
        }

        if (Input.touchCount == 1)
        {
            return TouchMode.Position;
        } else if (Input.touchCount == 2) {
            return TouchMode.RotateZoom;
        }
        return TouchMode.None;
    }

    //Used to be a toogle, but now only acts as a permanent Lock Button.
    public void TogglePositionLock(bool isLocked)
    {
        if (isLocked)
        {
            var planes = FindObjectOfType<ARPlaneManager>().trackables;
            foreach (var p in planes)
                p.gameObject.SetActive(false);

            var points = FindObjectOfType<ARPointCloudManager>().trackables;
            foreach (var p in points)
                p.gameObject.SetActive(false);

            steps.Done();
            FindObjectOfType<RaycastSelect>().enabled = true;
        }
        enabled = !isLocked;
        lockedUi.SetActive(isLocked);
        unlockedUi.SetActive(!isLocked);
        lockText.text = isLocked ? "Position Locked" : "Position Unlocked";
    }
}

enum TouchMode
{
    None,
    Position,
    RotateZoom
}