﻿using FluentFTP;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using TMPro;
using UnityEngine;

/// <summary>
/// Saves the Comments of a User.
/// </summary>
public class CommentManager : MonoBehaviour
{
    [SerializeField]
    private TMP_InputField nameInput, commentInput;

    public void Save()
    {
        FtpClient client = new FtpClient(ConfigLoader.config.defaultFtpUrl,
        new NetworkCredential(ConfigLoader.config.defaultFtpUsername, ConfigLoader.config.defaultFtpPassword));
        client.Connect();
        var cwd = Path.Combine(ConfigLoader.config.defaultFtpFolder, FindObjectOfType<LoaderManager>().projectName);
        client.SetWorkingDirectory(cwd);
        var commentData = new { name = nameInput.text, comment = commentInput.text, date = DateTime.Now.ToString()};
        var json = JsonConvert.SerializeObject(commentData);

        using(var stream = new MemoryStream(Encoding.UTF8.GetBytes(json)))
        {
            client.Upload(stream, $"cmnt_{nameInput.text}_{DateTime.Now:ddMMyy_HHmmss}.json", FtpExists.Overwrite);
        }

        client.Disconnect();
    }
}
