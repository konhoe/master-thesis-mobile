﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Threading.Tasks;
using UnityEditor;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Loads and provides the local configuration.
/// </summary>
[Serializable]
public static class ConfigLoader
{
  public static Config config = new Config();

  private static string configFile = "config.json";

  private static string persistentPath = Path.Combine(Application.persistentDataPath, configFile);
  private static string streamingPath = Path.Combine(Application.streamingAssetsPath, configFile);
  static ConfigLoader()
  {
    string configJson;
    if (File.Exists(persistentPath))
    {
      configJson = ReadJson(persistentPath);
      Debug.Log("Found Config in Persistent data path.");
    }
    else
    {
      configJson = ReadJson(streamingPath);
      Debug.Log("Found Config in StreamingAssets.");
    }

    config = JsonConvert.DeserializeObject<Config>(configJson);
  }

  internal static void SaveConfig()
  {
    var text = JsonConvert.SerializeObject(config);
    File.WriteAllText(persistentPath, text);
  }

  private static string ReadJson(string path)
  {
    var json = "";
    if (path.StartsWith("jar"))
    {
      using (UnityWebRequest www = UnityWebRequest.Get(path))
      {
        var asyncOp = www.SendWebRequest();
        while (asyncOp.isDone == false) { }
        json = www.downloadHandler.text;
      }
    }
    else
    {
      json = File.ReadAllText(path);
    }

    return json;
  }

}

/// <summary>
/// Represents a local configuration.
/// </summary>
public class Config
{
  public string defaultFtpUrl;
  public string defaultFtpFolder;
  public string defaultFtpPassword;
  public string defaultFtpUsername;

  public Config()
  {
  }
}