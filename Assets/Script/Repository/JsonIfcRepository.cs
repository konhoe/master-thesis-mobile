﻿using IfcEditorTypes;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

/// <summary>
/// Loads the project's JSON file, deserializes it into a IFC Export and provides its data.
/// </summary>
public class JsonIfcRepository
{
    private static readonly JsonIfcRepository _singleton = new JsonIfcRepository();

    private IfcExport ifcExport;

    public static JsonIfcRepository GetJsonIfcRepository()
    {
        return _singleton;
    }

    private JsonIfcRepository(){}

    public void Initialize(string pathToJson)
    {
        var json = System.IO.File.ReadAllText(pathToJson);
        var serializerSettings = new JsonSerializerSettings();

        ifcExport = JsonConvert.DeserializeObject<IfcExport>(json, serializerSettings);
    }

    public Vector3 GetPosition(string ifcFile)
    {
        IfcTransform transform = ifcExport.transforms.SingleOrDefault(t => t.target == ifcFile);
        if(transform.target == null)
            return Vector3.zero;
        else
            return transform.position;
    }

    public Quaternion GetRotation(string ifcFile)
    {
        IfcTransform transform = ifcExport.transforms.SingleOrDefault(t => t.target == ifcFile);
        if (transform.target == null)
            return Quaternion.identity;
        else
            return Quaternion.Euler(transform.rotation);
    }

    internal IfcElement GetElementById(string id)
    {
        return ifcExport.ifcElements.Find(elem => elem.id == id);
    }

    public Vector3 GetScale(string ifcFile)
    {
        IfcTransform transform = ifcExport.transforms.SingleOrDefault(t => t.target == ifcFile);
        if (transform.target == null)
            return Vector3.one;
        else
            return transform.scale;
    }

    public IEnumerable<Layer> GetAllLayers()
    {
        HashSet<Layer> toReturn = new HashSet<Layer>();
        foreach(var elem in ifcExport.ifcElements)
        {
            foreach(var layer in elem.layers)
            {
                toReturn.Add(layer);
            }
        }
        Debug.Log($"Json Contains {toReturn.Count} Layers.");
        return toReturn;
    }

    internal List<string> GetFilteredProps()
    {
      return ifcExport.filteredProperties;
    }

    public IEnumerable<string> GetGroups()
    {
        return ifcExport.layerGroups;
    }

    public IEnumerable<string> GetElementIdsOnLayer(string layer)
    {
        return ifcExport.ifcElements
            .FindAll(ifc => ifc.layers.FindAll(l => l.name == layer).Count > 0)
            .Select(elem => elem.id);
    }

    public List<Annotation> GetAllAnnotations()
    {
        return ifcExport.annotations;
    }

    public List<IfcElement> GetAllIfcElements()
    {
        return ifcExport.ifcElements;
    }

    public IEnumerable<string> GetAllIfcFiles()
    {
      return ifcExport.transforms.Select(f => f.target);
    }
}
