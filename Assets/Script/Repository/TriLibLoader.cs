﻿using System.Collections;
using System.Collections.Generic;
using TriLib;
using System.Linq;
using UnityEngine;

/// <summary>
/// We use the Unity Asset TriLib to load the geometry representation of a IFC file.
/// As this is a commercial library a licence needs to be aquired from the Unity Assetstore.
/// TODO: Replace Trilib with a open & free alternative (eg https://github.com/intelligide/assimp-unity or https://github.com/assimp/assimp-net)
/// </summary>
public class TriLibLoader {

    private readonly List<string> supportedExtensions = new List<string> { ".ifc", ".dae", ".obj", ".fbx" };

    public GameObject LoadModel(string file, GameObject parent)
    {
        var assetLoader = new AssetLoader();
        var assetLoaderOptions = AssetLoaderOptions.CreateInstance();

        assetLoaderOptions.PostProcessSteps =
            AssimpPostProcessSteps.CalcTangentSpace
            | AssimpPostProcessSteps.GenSmoothNormals
            | AssimpPostProcessSteps.ImproveCacheLocality
            | AssimpPostProcessSteps.LimitBoneWeights
            | AssimpPostProcessSteps.Triangulate
            | AssimpPostProcessSteps.GenUvCoords
            | AssimpPostProcessSteps.SortByPType
            | AssimpPostProcessSteps.FindInvalidData
            | AssimpPostProcessSteps.MakeLeftHanded
            | AssimpPostProcessSteps.FlipWindingOrder;
        assetLoaderOptions.RotationAngles = new Vector3(90, 0, 0);
        assetLoaderOptions.GenerateMeshColliders = true;
        GameObject loadedObject = assetLoader.LoadFromFileWithTextures(file, assetLoaderOptions, parent);
        Shader doubleSided = Shader.Find("Standard-DoubleSided");
        foreach (var r in loadedObject.GetComponentsInChildren<Renderer>())
        {
            r.material.shader = doubleSided;
            if (r.material.HasProperty("_Glossiness")) r.material.SetFloat("_Glossiness", 0.2f);
        }
        return loadedObject;
    }

    public bool canLoad(string fileExtension)
    {
        return supportedExtensions.Contains(fileExtension);
    }
}
