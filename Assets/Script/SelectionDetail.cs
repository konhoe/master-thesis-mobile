﻿using IfcEditorTypes;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.UIElements;

/// <summary>
/// Shows the information of a selected Object.
/// </summary>
public class SelectionDetail : MonoBehaviour
{
  public GameObject details;
  public TextMeshProUGUI detailHolder;

  private RaycastSelect sceneSelection;

  private JsonIfcRepository ifcRepository;
  private LoaderManager loaderManager;

  private void Start()
  {
    ifcRepository = JsonIfcRepository.GetJsonIfcRepository();
    loaderManager = FindObjectOfType<LoaderManager>();
    sceneSelection = FindObjectOfType<RaycastSelect>();
    sceneSelection.selectionChanged.AddListener(delegate { UpdateSelectionDetail(); });
  }

  private void UpdateSelectionDetail()
  {
    details.SetActive(true);

    // Scroll to top on new Selection
    var scroll = details.GetComponentInChildren<ScrollRect>();
    scroll.normalizedPosition = new Vector2(0, 1);

    detailHolder.text = "";
    var selected = sceneSelection.currentSelection;

    if (selected == null)
    {
      details.SetActive(false);
      return;
    }

    if (loaderManager.annotations.Contains(selected))
    {
      var selectedAnnotation = selected.GetComponent<AnnotationComponent>().annotation;
      detailHolder.text +=
                  $"<b>Annotation:</b> \n {selectedAnnotation.title} \n {selectedAnnotation.content}";
      return;
    }

    var id = loaderManager.GetIdFromGameObjectName(selected.name);
    if (id != null)
    {
      IfcElement element = ifcRepository.GetElementById(id);
      detailHolder.text += $"<b>{element.elementName}</b> \n";
      detailHolder.text += element.comment + "\n\n";
      if (element.properties != null)
      {
        foreach (var prop in element.properties)
        {
          if (!prop.isActive || ifcRepository.GetFilteredProps().Contains(prop.name)) continue;

          detailHolder.text += $"<b>{prop.name}</b> \n";
          foreach (var att in prop.attributes)
          {
            detailHolder.text += att + "\n";
          }
          detailHolder.text += "\n";
        }
      }
    }
    Canvas.ForceUpdateCanvases();
    var textParent = detailHolder.transform.parent.GetComponent<RectTransform>();
    textParent.sizeDelta = new Vector2(textParent.sizeDelta.x, detailHolder.GetComponent<RectTransform>().sizeDelta.y + 10);
  }

  public void IncreaseFontSize()
  {
    if (detailHolder.fontSize > 30) return;
    detailHolder.fontSize += 1;
  }

  public void DecreaseFontSize()
  {
    if (detailHolder.fontSize < 12) return;
    detailHolder.fontSize -= 1;
  }
}
