﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Handles the tutorial text throughout the application.
/// </summary>
public class StepTutorial : MonoBehaviour
{
    [SerializeField]
    private List<GameObject> steps;
    [SerializeField]
    private GameObject buttonHolder;

    private int currentStep = 0;

    public void Start()
    {
        ShowStep(0);
    }

    public void ShowStep(int number)
    {
        if (number < currentStep) return;
        currentStep = number;
        steps.ForEach(s => s.SetActive(false));
        steps[number].SetActive(true);
    }

    public void Done()
    {
        currentStep = steps.Count;
        steps.ForEach(s => s.SetActive(false));
        buttonHolder.SetActive(true);
    }
}
