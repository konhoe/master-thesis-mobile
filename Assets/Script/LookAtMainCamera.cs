﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Rotates transform towards Main Camera.
/// </summary>
public class LookAtMainCamera : MonoBehaviour
{
  [SerializeField] private bool x;
  [SerializeField] private bool y;
  [SerializeField] private bool z;

  private Transform mainCameraTransform;

  private void Start()
  {
    mainCameraTransform = Camera.main.transform;
  }

  void Update()
  {
    Vector3 look = mainCameraTransform.position - transform.position;
    var newRotation = Quaternion.LookRotation(look, Vector3.up).eulerAngles;
    if (!x)
      newRotation.x = 0;
    if (!y)
      newRotation.y = 0;
    if (!z)
      newRotation.z = 0;
    if (this.name.ToLower().Contains("flag")) Debug.Log("FLAG: " + newRotation);
    transform.rotation = Quaternion.Euler(newRotation);
  }
}
