﻿using IfcEditorTypes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Scene representation of an Annotaion.
/// </summary>
public class AnnotationComponent : MonoBehaviour
{
    public Annotation annotation;
    
    [SerializeField]
    private Canvas previewCanvas;
    private RaycastSelect sceneSelection;


    private void Awake()
    {
        sceneSelection = FindObjectOfType<RaycastSelect>();
        sceneSelection.selectionChanged.AddListener(() =>
        {
            if (sceneSelection.currentSelection == this.gameObject)
                Select();
            else
                Deselect();
        });
    }

    public void Select()
    {
        previewCanvas.gameObject.SetActive(true);
    }
    public void Deselect()
    {
        previewCanvas.gameObject.SetActive(false);
    }
}
