﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// Controls the layers and layer groups (buttons) in the project.
/// </summary>
public class LayerManager : MonoBehaviour
{
  [SerializeField]
  private Transform buttonHolder;

  [SerializeField]
  private GameObject buttonPrefab;

  private JsonIfcRepository repository;

  private List<LayerButton> buttons = new List<LayerButton>();
  private LoaderManager loaderManager;

  private void Start()
  {
    loaderManager = FindObjectOfType<LoaderManager>();
    loaderManager.onObjectLoad.AddListener(DisplayLayerButtons);
  }

  private void DisplayLayerButtons()
  {
    repository = JsonIfcRepository.GetJsonIfcRepository();
    GetLayerGroups();

    foreach (var b in buttons)
    {
      var newButton = Instantiate(buttonPrefab, buttonHolder);
      newButton.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = b.buttonName;
      newButton.GetComponent<Button>().onClick.AddListener(delegate
      {
        ShowElementsFromButton(b);
      });
    }
    if (buttons.Count > 0)
    {
      var newButton = Instantiate(buttonPrefab, buttonHolder);
      newButton.GetComponentInChildren<TMPro.TextMeshProUGUI>().text = "Show All";
      newButton.GetComponent<Button>().onClick.AddListener(delegate
      {
        ShowAllElements();
      });
    }
  }

  private void ShowAllElements()
  {
    loaderManager.ShowAllElements();
  }

  private void ShowElementsFromButton(LayerButton b)
  {
    foreach (var l in b.layers)
    {
      ShowElements(repository.GetElementIdsOnLayer(l));
      ToggleAnnotaionsOnLayer(l);
    }

  }

  private void GetLayerGroups()
  {
    var groups = repository.GetGroups();
    var layers = repository.GetAllLayers();
    foreach (var group in groups)
    {
      var newButton = new LayerButton(group);
      newButton.layers = layers.Where(l => l.groups.Contains(group)).Select(l => l.name).ToList();
      buttons.Add(newButton);
    }
  }

  private void ShowElements(IEnumerable<string> idList)
  {
    foreach (var g in loaderManager.loadedObjects)
    {
      Transform[] children = g.GetComponentsInChildren<Transform>();
      foreach (Transform t in children)
      {
        var id = loaderManager.GetIdFromGameObjectName(t.name);
        if (id == null && t.name.StartsWith("IfcMappedItem"))
        {
          id = loaderManager.GetIdFromGameObjectName(t.parent.name);
        }
        var renderer = t.GetComponent<Renderer>();
        if (renderer == null) continue;
        if (idList.Contains(id))
        {
          renderer.enabled = true;
        }
        else
        {
          renderer.enabled = false;
        }
      }
    }
  }

  internal void ToggleAnnotaionsOnLayer(string layer)
  {
    foreach (var a in loaderManager.annotations)
    {
      if (a.GetComponent<AnnotationComponent>().annotation.layers.Exists(l => l.name == layer))
      {
        a.SetActive(true);
      }
      else
      {
        a.SetActive(false);
      }
    }
  }
}


public class LayerButton
{
  public string buttonName;
  public List<string> layers = new List<string>();

  public LayerButton(string buttonName)
  {
    this.buttonName = buttonName;
  }
}