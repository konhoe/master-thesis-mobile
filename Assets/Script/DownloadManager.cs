﻿using System.Collections;
using System.Linq;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;

/// <summary>
/// User Interface to the FTP Downloader.
/// </summary>
public class DownloadManager : MonoBehaviour
{
  [SerializeField]
  private Transform downloadWindow;

  [SerializeField]
  private Transform downloadButtonHolder;

  [SerializeField]
  private GameObject downloadButtonPrefab;

  private FTPDownloader ftp;
  private FtpCredentialsManager ftpCredentials;
  private LoaderManager loader;

  // Start is called before the first frame update
  void Start()
  {
    ftp = FindObjectOfType<FTPDownloader>();
    ftpCredentials = FindObjectOfType<FtpCredentialsManager>();
    loader = FindObjectOfType<LoaderManager>();
    Refresh();
  }

  public void Refresh()
  {
    foreach (Transform t in downloadButtonHolder)
      Destroy(t.gameObject);

    var projectsWithDate = ftp.GetDirectoryItems().OrderByDescending(dir => dir.Modified);
    var remoteProjects = ftp.ListRemoteProjects();
    var localProjects = ftp.ListLocalProjects().OrderByDescending(name => {
      var matchingServerProjects = projectsWithDate.Where(dirWithDate => dirWithDate.Name == name);
      if (matchingServerProjects.Count() > 0)
        return matchingServerProjects.First().Modified;
      else
        return new DateTime();
    });

    foreach (var projectName in localProjects)
    {
      var projectDate = projectsWithDate.FirstOrDefault(i => i.Name == projectName)?.Modified.ToString("dd.MM.yyyy");
      var local = Instantiate(downloadButtonPrefab, downloadButtonHolder);
      var uiText = local.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
      uiText.text = projectName + (projectDate != null ? $" - <i>{projectDate}</i>" : "");

      var openButton = local.GetComponent<Button>();
      openButton.onClick.AddListener(delegate
      {
        loader.LoadProject(ftp.GetLocalPathOfProject(projectName));
        downloadWindow.gameObject.SetActive(false);
      });

      var deleteBtn = local.transform.Find("Delete").GetComponent<Button>();
      deleteBtn.onClick.AddListener(delegate
      {
        ftp.DeleteLocalProject(projectName);
        Refresh();
      });
    }

    var onlyRemoteProjects = projectsWithDate.Where(p => !localProjects.Contains(p.Name));

    foreach (var p in onlyRemoteProjects)
    {
      var remote = Instantiate(downloadButtonPrefab, downloadButtonHolder);
      var uiText = remote.transform.GetChild(0).GetComponent<TextMeshProUGUI>();
      uiText.text = $"{p.Name}  - <i>{p.Modified:dd.MM.yyyy}</i>";
      uiText.color = Color.gray;

      var downloadBtn = remote.GetComponent<Button>();
      downloadBtn.onClick.AddListener(delegate
      {
        StartCoroutine(DownloadProject(uiText, p.Name));
      });
      remote.transform.Find("Delete").gameObject.SetActive(false);
    }
  }

  public void SaveAndConnect()
  {
    ftpCredentials.SaveCurrentCredentials();
    Refresh();
  }

  private IEnumerator DownloadProject(TextMeshProUGUI text, string p)
  {
    text.text = "Downloading...";
    yield return 0;
    ftp.DownloadProject(p);
    Refresh();
  }
}
