﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// Shows the attached media of a annotaion.
/// </summary>
public class MediaPreviewAnnotation : MonoBehaviour
{
    [SerializeField]
    private GameObject playIndicator;
    [SerializeField]
    private RawImage preview;
    [SerializeField]
    private VideoPlayer videoPlayer;

    private MediaPlayer fullscreenPlayer;

    private List<string> imageExtension = new List<string> { ".jpg", ".jpeg", ".png", ".gif" };
    private List<string> videoExtension = new List<string> { ".mp4" };
    private string path;

    void Start()
    {
        path = GetComponentInParent<AnnotationComponent>().annotation.media;
        fullscreenPlayer = FindObjectOfType<MediaPlayer>();

        if (string.IsNullOrEmpty(path))
            this.gameObject.SetActive(false);
        else
            LoadPreview();
    }



    private void LoadPreview()
    {
        gameObject.SetActive(true);
        StartCoroutine(LoadMedia(path));
    }

    private IEnumerator LoadMedia(string path)
    {
        var ext = Path.GetExtension(path);
        if (imageExtension.Contains(ext.ToLower()))
        {
            Debug.Log("MediaFile is an Image");
            playIndicator.SetActive(false);

            using (UnityWebRequest loader = UnityWebRequestTexture.GetTexture("file://" + path))
            {
                yield return loader.SendWebRequest();
                if (string.IsNullOrEmpty(loader.error))
                {
                    var tex = DownloadHandlerTexture.GetContent(loader);
                    var sprite = Sprite.Create(tex, new Rect(Vector2.zero, new Vector2(tex.width, tex.height)), new Vector2(0.5f, 0.5f));
                    preview.texture = sprite.texture;
                    Debug.Log("Finished Loading Image");
                }
            }
        }
        else if (videoExtension.Contains(ext.ToLower()))
        {
            Debug.Log("MediaFile is a Video");
            playIndicator.SetActive(true);
            preview.texture = videoPlayer.targetTexture;
            videoPlayer.url = "file://" + path;
            videoPlayer.prepareCompleted += (vid) =>
                {
                    videoPlayer.time = 1.0f;
                    videoPlayer.Play();
                    videoPlayer.Pause();
                    Debug.Log("Finished Loading Video.");
                };
            videoPlayer.Prepare();
        }
    }

    public void OpenFullscreen()
    {
        Debug.Log("Opening Fullscreen MediaPlayer");
        fullscreenPlayer.gameObject.SetActive(true);
        fullscreenPlayer.LoadMedia(path);
    }
}
