﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
using UnityEngine.Video;

/// <summary>
/// Shows and plays the media of an annotaion.
/// </summary>
public class MediaPlayer : MonoBehaviour
{

    [SerializeField]
    private RawImage videoTarget;
    [SerializeField]
    private Image image;

    [SerializeField]
    private VideoPlayer player;
    [SerializeField]
    private SVGImage playPause;
    [SerializeField]
    private Sprite play, pause;

    private List<string> imageExtension = new List<string> { ".jpg", ".jpeg", ".png", ".gif" };
    private List<string> videoExtension = new List<string> { ".mp4" };

    public void LoadMedia(string path)
    {
        player.gameObject.SetActive(true);
        StartCoroutine(PrepareMedia(path));
    }

    private IEnumerator PrepareMedia(string path)
    {
        var ext = Path.GetExtension(path);
        if (imageExtension.Contains(ext.ToLower()))
        {
            Debug.Log("MediaFile is an Image");
            using (UnityWebRequest loader = UnityWebRequestTexture.GetTexture("file://" + path))
            {
                yield return loader.SendWebRequest();
                if (string.IsNullOrEmpty(loader.error))
                {
                    var tex = DownloadHandlerTexture.GetContent(loader);
                    prepareImage(tex);
                }
            }
        }
        else if (videoExtension.Contains(ext.ToLower()))
        {
            prepareVideo(path);
        }

    }

    public void PlayPause()
    {
        if (player.isPaused)
        {
            player.Play();
            playPause.sprite = pause;
        }
        else
        {
            player.Pause();
            playPause.sprite = play;
        }
    }

    public void Jump( int amount)
    {
        player.time = player.time + amount;
        player.Play();
    }
    private void prepareImage(Texture2D tex)
    {
        var sprite = Sprite.Create(tex, new Rect(Vector2.zero, new Vector2(tex.width, tex.height)), new Vector2(0.5f, 0.5f));
        image.sprite = sprite;
        image.preserveAspect = true;
        image.gameObject.SetActive(true);
        videoTarget.gameObject.SetActive(false);
        Debug.Log("Finished Loading Image");
    }

    private void prepareVideo(string path)
    {
        Debug.Log("MediaFile is a Video");
        videoTarget.gameObject.SetActive(true);

        player.url = "file://" + path;
        player.prepareCompleted += (_) =>
        {
            player.Play();
            playPause.sprite = pause;
            Debug.Log("Finished Loading Video.");
        };
        player.Prepare();
    }
}
