﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

/// <summary>
/// Checks wether the system supports ARCore and disables the ARCore Button if it isn't supported.
/// </summary>
public class ArSupport : MonoBehaviour
{
    [SerializeField]
    private Button arcore;
    [SerializeField] 
    private ARSession m_Session;
    IEnumerator Start()
    {
        if ((ARSession.state == ARSessionState.None )||
            (ARSession.state == ARSessionState.CheckingAvailability))
        {
            yield return ARSession.CheckAvailability();
        }

        if (ARSession.state == ARSessionState.Unsupported)
        {
            arcore.interactable = false;
            arcore.GetComponentInChildren<TextMeshProUGUI>().text = "AR Core not available";
        }
        else
        {
            arcore.interactable = true;
        }
    }

}
