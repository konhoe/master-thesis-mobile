﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Customizes the devices screen timeout.
/// </summary>
public class ScreenTimeout : MonoBehaviour
{
    [SerializeField] private int timeout;
    void Awake()
    {
        DontDestroyOnLoad(this);
        Screen.sleepTimeout = timeout;
    }
}
