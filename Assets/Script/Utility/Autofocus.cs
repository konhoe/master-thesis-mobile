﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

/// <summary>
/// Activates the cameras Autofocus to allow better target recogniton for Vuforia.
/// </summary>
public class Autofocus : MonoBehaviour
{
    void Start()
    {
        Debug.Log("Setting Autofocus");
        CameraDevice.Instance.SetFocusMode(CameraDevice.FocusMode.FOCUS_MODE_CONTINUOUSAUTO);
    }
}
