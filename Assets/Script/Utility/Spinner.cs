﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Simple Loading spinner that rotates its transform around the forward direction. 
/// </summary>
public class Spinner : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        var mod = (Mathf.Sin(Time.time*2) + 1) * 20;
        transform.Rotate(Vector3.forward, -Time.deltaTime * 20 * mod);
    }
}
