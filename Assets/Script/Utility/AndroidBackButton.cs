﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Allows switching back to thew main scene or quitting the application through the systems back button.
/// </summary>
public class AndroidBackButton : MonoBehaviour
{
    [SerializeField]
    private string mainScene = "main";

    void Start()
    {
        DontDestroyOnLoad(this);
    }

    // OnBackButton go Back to main Scene, if in main scene Quit
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) {
            if (SceneManager.GetActiveScene().name == mainScene)
            {
                Application.Quit();
                return;
            }
            SceneManager.LoadScene(mainScene);
        }
    }
}
