﻿using FluentFTP.Proxy;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

/// <summary>
/// Sets the FTP Connection Input fields and handles their changes.
/// </summary>
public class FtpCredentialsManager : MonoBehaviour
{
  public UnityEvent OnCredentialsChange;

  [SerializeField]
  private List<TMP_InputField> hostInputs, folderInputs, usernameInputs, passwordInputs;

  private FTPDownloader ftpDownloader;
  private Config config;

  private void Awake()
  {
    config = ConfigLoader.config;
    ftpDownloader = FindObjectOfType<FTPDownloader>();

    hostInputs.ForEach(i => i.onEndEdit.AddListener(text =>
    {
      config.defaultFtpUrl = text;
      SyncInputs(hostInputs, text);
      ftpDownloader.LoadConfigConnect();
      OnCredentialsChange.Invoke();
    }));
    folderInputs.ForEach(i => i.onEndEdit.AddListener(text =>
    {
      config.defaultFtpFolder = text;
      SyncInputs(folderInputs, text);
      ftpDownloader.LoadConfigConnect();
      OnCredentialsChange.Invoke();
    }));
    usernameInputs.ForEach(i => i.onEndEdit.AddListener(text =>
    {
      config.defaultFtpUsername = text;
      SyncInputs(usernameInputs, text);
      ftpDownloader.LoadConfigConnect();
      OnCredentialsChange.Invoke();
    }));
    passwordInputs.ForEach(i => i.onEndEdit.AddListener(text =>
    {
      config.defaultFtpPassword = text;
      SyncInputs(passwordInputs, text);
      ftpDownloader.LoadConfigConnect();
      OnCredentialsChange.Invoke();
    }));
    UpdateInputs();
  }

  private void UpdateInputs()
  {
    SyncInputs(hostInputs, config.defaultFtpUrl);
    SyncInputs(folderInputs, config.defaultFtpFolder);
    SyncInputs(usernameInputs, config.defaultFtpUsername);
    SyncInputs(passwordInputs, config.defaultFtpPassword);
  }

  private void SyncInputs(List<TMP_InputField> inputs, string text)
  {
    inputs.ForEach(i => i.SetTextWithoutNotify(text));
    Canvas.ForceUpdateCanvases();
  }

  internal void SaveCurrentCredentials()
  {
    ConfigLoader.SaveConfig();
  }
}
