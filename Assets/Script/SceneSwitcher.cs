﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneSwitcher : MonoBehaviour
{
    [SerializeField]
    private GameObject loading;

    public void LoadScene(string sceneName)
    {
        SceneManager.LoadScene(sceneName);
    }

    public void ReloadCurrentScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void LoadSceneAsync(string sceneName)
    {
        StartCoroutine(LoadingScene(sceneName));
    }
    private IEnumerator LoadingScene(string sceneName)
    {
        DontDestroyOnLoad(this.gameObject);
        DontDestroyOnLoad(loading);
        var asyncLoad = SceneManager.LoadSceneAsync(sceneName,LoadSceneMode.Single);
        while(!asyncLoad.isDone)
        {
            yield return 0;
        }
        Destroy(loading);
        Destroy(this);
    }
}
