﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

/// <summary>
/// Handles selection of objects in the scene.
/// </summary>
public class RaycastSelect : MonoBehaviour
{

  public GameObject currentSelection;
  public UnityEvent selectionChanged;

  [SerializeField]
  private Color hightlightColor = Color.red;
  private int depth = 0;
  private Color originalColor;
  private LoaderManager loader;

  private void Start()
  {
    loader = FindObjectOfType<LoaderManager>();
  }

  void Update()
  {
    if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
      RaycastSelection();
  }

  private void RaycastSelection()
  {
    if (EventSystem.current.IsPointerOverGameObject(Input.touches[0].fingerId)) return;
    DeselectAll();

    Debug.Log("Casting Ray");
    List<Transform> hits = new List<Transform>();
    Ray ray = Camera.main.ScreenPointToRay(Input.GetTouch(0).position);

    //Order List by distance to Camera and get List of Transforms
    hits = Physics.RaycastAll(ray).OrderBy(x => x.distance).Select((elem) => elem.transform).ToList();

    if (hits.Count == 0)
    {
      NoSelection();
      return;
    }

    //Step one depth down if raycast hit same object again
    if (currentSelection != null && hits.Contains(currentSelection.transform))
      depth = Mathf.Clamp(depth + 1, 0, hits.Count - 1);
    else
      depth = 0;

    //if same Object was selected agian => Deselect
    if (currentSelection == hits[depth].gameObject)
      NoSelection();

    currentSelection = hits[depth].gameObject;
    MarkObjectSelected(currentSelection);
    Debug.Log("Selection Changed to: " + currentSelection);
    selectionChanged.Invoke();
  }

  private void NoSelection()
  {
    Debug.Log("Casting Ray - No Hits");
    depth = 0;
    currentSelection = null;
    selectionChanged.Invoke();
  }

  private void MarkObjectSelected(GameObject g)
  {
    Material mat = g.transform.GetComponentInChildren<MeshRenderer>()?.material;
    originalColor = mat.color;
    mat.SetColor("_EmissionColor", hightlightColor);
    mat.EnableKeyword("_EMISSION");
  }

  private void DeselectAll()
  {
    foreach (var loaded in loader.loadedObjects)
    {
      var meshes = loaded.GetComponentsInChildren<MeshRenderer>();
      foreach (var m in meshes)
      {
        m.material.SetColor("_EmissionColor", Color.white);
        m.material.DisableKeyword("_EMISSION");
      }
    }
    foreach (var a in loader.annotations)
    {
      var meshes = a.GetComponentsInChildren<MeshRenderer>();
      foreach (var m in meshes)
      {
        m.material.SetColor("_EmissionColor", Color.white);
        m.material.DisableKeyword("_EMISSION");
      }
    }
  }

}
