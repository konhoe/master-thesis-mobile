﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;

/// <summary>
/// Handles loading of IFC files and the projects JSON file.
/// </summary>
public class LoaderManager : MonoBehaviour
{
  public UnityEvent onObjectLoad;
  [SerializeField]
  private Transform spawnParent;
  [SerializeField]
  private GameObject annotationBallPrefab, annotationFlagPrefab;
  [SerializeField]
  private float scaleFactor = 1;


  public List<GameObject> loadedObjects = new List<GameObject>();
  public List<GameObject> annotations = new List<GameObject>();
  public string projectName;
  private StepTutorial steps;
  private TriLibLoader triLibLoader = new TriLibLoader();
  private JsonIfcRepository ifcRepository = JsonIfcRepository.GetJsonIfcRepository();
  private const int ID_LENGTH = 22;

  private bool usesVuforia;

  private void Start()
  {
    steps = FindObjectOfType<StepTutorial>();
  }

  public void LoadProject(string path)
  {
    projectName = Path.GetFileNameWithoutExtension(path);
    var allFiles = Directory.GetFiles(path);
    var ifcFiles = allFiles.Where(item => item.ToLower().EndsWith(".ifc"));
    var jsonFile = allFiles.Single(item => Path.GetFileName(item).StartsWith(projectName));

    ifcRepository.Initialize(jsonFile);

    foreach (var file in ifcFiles)
    {
      var objectName = Path.GetFileName(file);

      var objectParent = new GameObject(objectName);
      objectParent.transform.parent = spawnParent;

      var newObject = triLibLoader.LoadModel(file, objectParent);
      CenterRoot(newObject.transform);
      UniformScale(newObject.transform);

      var newPosition = ifcRepository.GetPosition(objectName);
      var newRotation = ifcRepository.GetRotation(objectName);
      var newScale = ifcRepository.GetScale(objectName);
      objectParent.transform.SetPositionAndRotation(newPosition, newRotation);
      objectParent.transform.localScale = newScale;

      loadedObjects.Add(newObject);
    }

    var jsonAnnotations = ifcRepository.GetAllAnnotations();
    foreach (var a in jsonAnnotations)
    {
      if (!string.IsNullOrEmpty(a.media))
        a.media = Path.Combine(path, a.media);
      var newAnnotation = Instantiate(a.visual == "ball" ? annotationBallPrefab : annotationFlagPrefab , spawnParent);
      newAnnotation.transform.position = a.position;
      newAnnotation.transform.localScale = new Vector3(a.size, a.size, a.size);
      newAnnotation.transform.rotation = Quaternion.identity;
      newAnnotation.name = a.title;
      newAnnotation.GetComponentInChildren<TMP_Text>(true).text = a.title;
      var component = newAnnotation.GetComponent<AnnotationComponent>();
      component.annotation = a;

      annotations.Add(newAnnotation);
    }

    //remove Deleted IfcElements
    foreach (var ifcElement in ifcRepository.GetAllIfcElements().Where(elem => elem.deleted))
    {
      Destroy(GetGameObjectById(ifcElement.id));
    }


    //foreach (var ifcElement in ifcRepository.GetAllIfcElements())
    //{
    //  Debug.Log($"{ifcElement.elementName} has {ifcElement.id}");
    //}

    //foreach (var t in loadedObjects[0].GetComponentsInChildren<Transform>())
    //{
    //  Debug.Log($"GameObject {t.name}");
    //}

    //Vuforia expects disabled Renderes and enables them OnTargetFound
    usesVuforia = FindObjectOfType<Vuforia.VuforiaBehaviour>() != null;
    if (usesVuforia)
    {
      DisableRenderers(loadedObjects);
      DisableRenderers(annotations);
    }

    spawnParent.localScale *= scaleFactor;
    onObjectLoad.Invoke();
    if (steps != null) steps.ShowStep(1);
  }

  internal void ShowAllElements()
  {
    foreach (var g in loadedObjects)
    {
      var renderers = g.GetComponentsInChildren<Renderer>();
      foreach (var r in renderers)
        r.enabled = true;
    }
    foreach (var a in annotations)
    {
      a.gameObject.SetActive(true);
    }
  }

  private void DisableRenderers(List<GameObject> objects)
  {
    foreach (var g in objects)
    {
      var renderers = g.GetComponentsInChildren<Renderer>();
      foreach (var r in renderers)
        r.enabled = false;
    }
  }

  private void CenterRoot(Transform root)
  {
    Vector3 center = new Vector3();
    if (root.childCount > 0)
    {
      Renderer[] allChildren = root.GetComponentsInChildren<Renderer>();
      foreach (Renderer child in allChildren)
      {
        center += child.bounds.center;
      }
      center /= allChildren.Length;
    }
    root.position -= new Vector3(center.x, 0, center.z);
  }

  private void UniformScale(Transform root)
  {
    Bounds bounds = new Bounds();
    if (root.childCount == 0) return;
    Renderer[] allChildren = root.GetComponentsInChildren<Renderer>();
    foreach (Renderer child in allChildren)
    {
      bounds.Encapsulate(child.bounds);
    }
    var avgBound = (bounds.size.x + bounds.size.y + bounds.size.z) / 3;
    ScaleMeshes(root, avgBound);
    Debug.Log("Scaling object by " + root.parent.localScale);
  }

  private void ScaleMeshes(Transform root, float scale)
  {
    MeshFilter[] allChildren = root.GetComponentsInChildren<MeshFilter>();
    foreach (MeshFilter child in allChildren)
    {
      var original = child.sharedMesh.vertices;
      var vertices = new Vector3[child.sharedMesh.vertices.Length];
      for (var i = 0; i < vertices.Length; i++)
      {
        vertices[i] = original[i] / scale;
      }
      child.sharedMesh.vertices = vertices;
      child.sharedMesh.RecalculateNormals();
      child.sharedMesh.RecalculateBounds();
      child.sharedMesh.Optimize();
      child.GetComponent<MeshCollider>().sharedMesh = child.sharedMesh;
      child.mesh = child.sharedMesh;
    }
    Transform[] children = root.GetComponentsInChildren<Transform>();
    foreach (var t in children)
    {
      t.localPosition /= scale;
    }
  }

  public string GetIdFromGameObjectName(string name)
  {
    if (name.Length <= ID_LENGTH) return null;
    if (name[name.Length - ID_LENGTH - 1] != '_') return null;

    return name.Substring(name.Length - ID_LENGTH);
  }

  public GameObject GetGameObjectById(string id)
  {
    foreach (var g in loadedObjects)
    {
      Transform[] children = g.GetComponentsInChildren<Transform>();
      for (int i = 0; i < children.Length; i++)
      {
        if (children[i].name.Contains(id))
          return children[i].gameObject;
      }
    }
    Debug.Log(id + " not found!");
    return null;
  }
}