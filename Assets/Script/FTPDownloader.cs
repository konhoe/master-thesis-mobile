﻿using FluentFTP;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using UnityEngine;

/// <summary>
/// Manages the connection to the FTP Server, allows downloading Projects, handles the local storage of projects.
/// </summary>
public class FTPDownloader : MonoBehaviour
{
  FtpClient client;
  private string storage;

  private void Awake()
  {
    storage = Path.Combine(Application.persistentDataPath, "ifcDownload");
    if (!Directory.Exists(storage))
      Directory.CreateDirectory(storage);

    LoadConfigConnect();
  }

  internal void LoadConfigConnect()
  {
    if (client != null && client.IsConnected) client.Disconnect();
    
    var config = ConfigLoader.config;
    client = new FtpClient(config.defaultFtpUrl,
        new NetworkCredential(config.defaultFtpUsername, config.defaultFtpPassword));
    client.Connect();
    client.SetWorkingDirectory(config.defaultFtpFolder);
  }

  internal IEnumerable<FtpListItem> GetDirectoryItems()
  {
    return client.GetListing();
  }

  public IEnumerable<string> ListRemoteProjects()
  {
    return client.GetListing().Select(item => item.Name);
  }

  public IEnumerable<string> ListLocalProjects()
  {
    return Directory.GetDirectories(storage).Select(fullPath => Path.GetFileName(fullPath));
  }

  public string DownloadProject(string project)
  {
    var files = client.GetNameListing(project);
    var path = Path.Combine(storage, project);

    if (!Directory.Exists(path))
    {
      Directory.CreateDirectory(path);
    }
    else
    {
      Directory.Delete(path, true);
      Directory.CreateDirectory(path);
    }

    client.DownloadFiles(path, files, true);
    return path;
  }

  internal void DeleteLocalProject(string project)
  {
    var path = Path.Combine(storage, project);
    Directory.Delete(path, true);
  }

  public string GetLocalPathOfProject(string project)
  {
    return Path.Combine(storage, project);
  }

  private void OnDestroy()
  {
    client.Disconnect();
  }
}
