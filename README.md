# IFC AR - Masters thesis

A Unity 3D android application that allows viewing special IFC projects exported by the [IFC Editor Application](https://gitlab.com/konhoe/master-thesis) in augmented reality for the use in educational settings.

 